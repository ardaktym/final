package kz.aitu.advancedJava.startingPoint.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "customer")
public class Customer {
    @Id
    private long id;
    private String name;
    private String address;
    private String phone;
    private String email;
    private String login;
    private String password;
}
