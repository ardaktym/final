package kz.aitu.advancedJava.startingPoint.services;

import kz.aitu.advancedJava.startingPoint.model.Category;
import kz.aitu.advancedJava.startingPoint.model.Product;
import kz.aitu.advancedJava.startingPoint.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ProductService {
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Iterable<Product> findAll(){
        return productRepository.findAll();
    }

    public Optional<Product> findById(long id) { return  productRepository.findById(id); }

    public List<Product> findAllByCategoryId(long categoryId) {return productRepository.findAllByCategoryId(categoryId); }
}
