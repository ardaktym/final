package kz.aitu.advancedJava.startingPoint.services;


import kz.aitu.advancedJava.startingPoint.model.Auth;
import kz.aitu.advancedJava.startingPoint.model.Order;
import kz.aitu.advancedJava.startingPoint.model.OrderItem;
import kz.aitu.advancedJava.startingPoint.repository.AuthRepository;
import kz.aitu.advancedJava.startingPoint.repository.OrderRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    private final OrderRepository orderRepository;
    private final AuthRepository authRepository;

    public OrderService(OrderRepository orderRepository, AuthRepository authRepository) {
        this.orderRepository = orderRepository;
        this.authRepository = authRepository;
    }

    public Iterable<Order> findAll(){
        return orderRepository.findAll();
    }

    public long createOrder(String token, List<OrderItem> orderItemList)  {
        Auth a = authRepository.findByToken(token);
        int totalPrice = findTotalPrice(orderItemList);
        String status = "Received";
        LocalDate date = LocalDate.now();
        orderRepository.insertOrder(a.getCustomerId(),date,totalPrice,status);
        List<Order> list = (List<Order>) orderRepository.findAll();
        return list.get(list.size()-1).getId();
    }

    public int findTotalPrice(List<OrderItem> orderItemList){
        int totalPrice = 0;
        for (int i = 0; i < orderItemList.size(); i++){
            totalPrice = totalPrice + (orderItemList.get(i).getPrice()*orderItemList.get(i).getQuantity());
        }
        return totalPrice;
    }

    public List<Order> getMyOrders(String token) {
        Auth a = authRepository.findByToken(token);
        List<Order> orders = new ArrayList<Order>();
        for(Order o: orderRepository.findAll() ){
            if (o.getCustomerId() == a.getCustomerId()){
                orders.add(o);
            }
        }
        return orders;
    }


    public List<Order> findAllByStatusId(String status) {
        return orderRepository.getAllByStatus(status);
    }


    public Object updateStatusByOrderId(Long orderId) {
        Optional<Order> order = orderRepository.findById(orderId);
        Order o = order.get();

        if (o.getStatus().equals("Received")) {
            o.setStatus("In-Progress");
        } else {
            o.setStatus("Done");
        }

        return orderRepository.save(o);
    }

    public void delete(Long orderId) {
        //Optional<Order> order = orderRepository.findById(orderId);
        //LocalDate d1 = order.get().getDate();
        //LocalDate d2 = LocalDate.now();
        //if(d1.compareTo(d2) > 7){
        orderRepository.deleteById(orderId);
        //}

    }
}
