package kz.aitu.advancedJava.startingPoint.repository;

import kz.aitu.advancedJava.startingPoint.model.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
    List<Product> findAllByCategoryId(long categoryId);
}
